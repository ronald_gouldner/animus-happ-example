$(document).ready(function () {
    $("form input[type=submit]").click(function (e) {
    	var name = $("form input[type=text]").val();
    	e.preventDefault();
    	$.ajax({
            type: 'POST',
            url: "/rest/happs-p/com.animushome.heart.happs.examplehapp",
            data: name,
            success: function (data) {
                alert("Success! This has also toggled the lights (if any are available)");
            },
            error: function (e) {
                alert("error! " + e.responseText)
            }
        });
    });
});