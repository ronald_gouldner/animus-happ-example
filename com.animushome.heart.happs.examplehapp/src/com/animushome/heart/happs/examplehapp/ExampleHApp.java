package com.animushome.heart.happs.examplehapp;

import java.util.HashMap;

import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.*;

import com.animushome.heart.service.HApp;
import com.animushome.heart.service.dal.Function;
import com.animushome.heart.service.dal.FunctionEvent;
import com.animushome.heart.service.dal.functions.BooleanControl;
import com.animushome.heart.service.dal.functions.Types;
import com.animushome.heart.service.dal.impl.HAppImpl;


/**
 * Main class for a hApp.
 * 
 * This is the main class that will register the hApp to the framework.
 * 
 * The minimum requirement when developing a hApp is to have this class implemented.
 *
 */
@Component(immediate = true, service={HApp.class, ExampleHAppService.class})
public class ExampleHApp extends HAppImpl implements ExampleHAppService {
	
	/**
	 * This is the unique ID of our hApp. 
	 */
	public final static String HAPP_UID = "com.animushome.heart.happs.examplehapp";
	
	
	/**
	 * This is a Function that will be published so that others can control our hApp
	 */
	private ServiceRegistration<Function> myLevelController;
	
	
	/**
	 * These are all the light Functions (not the devices) that we want to control 
	 */
	private HashMap<String, BooleanControl> lights = new HashMap<>();
	
	
	/**
	 * Activate is the first method that is called from the framework when the component has been satisfied. 
	 * @param context
	 */
	@Activate
	protected void activate(ComponentContext context) {
		construct(HAPP_UID, context);
		MyLevelController mlc = new MyLevelController(HAPP_UID, "f1", this);
		myLevelController = registerFunction(mlc);
	}
	
	
	/**
	 * Do whatever is necessary after the hApp as been deactivated. Close threads etc.
	 * @param context
	 */
	@Deactivate
	protected void deactivate(ComponentContext context) {
		logger.info("Deactivating Simple hApp");
	}

	
	/**
	 * This is how we get access to lights
	 * 
	 * We use Declarative Service to tell the framework that we want to have a reference to the following Functions
	 * so that we can control them. We can also specify a target to only get access of Lights and not all the other 
	 * BooleanControllers as well. We send an LDAP filter to the target which can be used to filter on even more attributes.
	 * 
	 * The lights will be added dynamically during runtime. If the a new light is added to the system the hApp will
	 * automatically pick it up. No need to restart the hApp.
	 * 
	 */
	@Reference(
			cardinality = ReferenceCardinality.MULTIPLE, 
			policy = ReferencePolicy.DYNAMIC, 
			target = "(" + Function.SERVICE_TYPE + "=" + Types.LIGHT + ")")
	public void setBooleanControl(BooleanControl bc) {
		lights.put(bc.getUID(), bc);
	}
	
	/**
	 * Handle any light that is removed from the framework
	 * 
	 * @param bc
	 */
	public void unsetBooleanControl(BooleanControl bc) {
		lights.remove(bc.getUID());
	}
	
	/**
	 * Method from our service interface to toggle all the lights
	 */
	@Override
	public void toggleLights() {
		logger.info("Toggle all the lights");
		lights.forEach((uid, bc)-> {
			try {
				bc.inverse();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}
	
	/**
	 * Method from our service interface to turn on all the lights
	 */
	@Override
	public void turnOnLights() {
		logger.info("Turn on all the lights");
		lights.forEach((uid, bc)-> {
			try {
				bc.setTrue();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
	}
	
	/**
	 * Method from our service interface to turn off all the lights
	 */
	@Override
	public void turnOffLights() {
		logger.info("Turn off all the lights");
		lights.forEach((uid, bc)-> {
			try {
				bc.setFalse();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
	}
	
	
	/**
	 * If we wish to listen for Functions when they update their states 
	 * E.g. when a sensor detects motion we will get that information in
	 * this method.
	 */
	@Override
	public void handleFunctionEvent(FunctionEvent e) {
		//Handle all incoming events
		
	}

}
