package com.animushome.heart.happs.examplehapp;

import java.math.BigDecimal;

import com.animushome.heart.service.Package;
import com.animushome.heart.service.dal.DeviceException;
import com.animushome.heart.service.dal.functions.data.LevelData;
import com.animushome.heart.service.dal.functions.impl.MultiLevelControlF;

/**
 * Function so that the GUI and other hApps can control our hApp. 
 * 
 * Similar to devices that can have Functions a hApp can also provide
 * Functions to show on the GUI or to be controlled by other hApps.
 * 
 * In this example we have a MultiLevelController to set a value between 0-100.
 *
 */
public class MyLevelController extends MultiLevelControlF{
	private ExampleHApp happ; 
	public MyLevelController(String device_UID, String function_UID, Package p) {
		super(device_UID, function_UID, p);
		this.happ = (ExampleHApp)p;
		data = new LevelData(0, null, BigDecimal.ZERO, null);
	}

	@Override
	public LevelData getData() throws DeviceException {
		return data;
	}

	@Override
	public void setData(BigDecimal level, String unit) throws DeviceException {
		data = new LevelData(System.currentTimeMillis(), null, level, null);
		//If level is greater then 50 turn on lights else turn off
		if(level.compareTo(BigDecimal.valueOf(50))>0){
			happ.turnOnLights();
		} else {
			happ.turnOffLights();
		}
		
	}

}
