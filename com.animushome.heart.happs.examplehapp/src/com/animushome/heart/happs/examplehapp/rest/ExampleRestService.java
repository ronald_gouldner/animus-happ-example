package com.animushome.heart.happs.examplehapp.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.knopflerfish.service.log.LogRef;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.animushome.heart.happs.examplehapp.ExampleHApp;
import com.animushome.heart.happs.examplehapp.ExampleHAppService;
import com.animushome.heart.service.HApp;

/**
 * This is our REST service that will be available on http://
 * <heart_ip>/rest/happs-p/com.animushome.heart.happs.examplehapp/
 * 
 * We can do a GET and POST request that will be digested by this service.
 * 
 * It's possible to use GET, POST, UPDATE, DELETE by adding the annotation to a
 * method.
 *
 */
@Path(HApp.HAPP_REST_URI + "/" + ExampleHApp.HAPP_UID)
@Component(immediate = true, service = ExampleRestService.class)
public class ExampleRestService {

	LogRef logger;
	private BundleContext bc;

	/**
	 * We use Declarative Service to get access to our hApp Service.
	 */
	@Reference
	ExampleHAppService shs;

	/**
	 * Activate is the first method that is called from the framework when this
	 * REST component has been satisfied.
	 * 
	 */
	@Activate
	void activate(BundleContext context) {
		this.bc = context;
		logger = new LogRef(bc);
		logger.info("activate() - starting up rest for example hApp");
	}

	@GET
	public String getForm() {
		return "REST service for Example hApp";
	}

	/**
	 * Handle all POST request on this end point. If the request name matches
	 * 'heart' then we will call our hApp service to toggle the lights.
	 * 
	 * @param name
	 * @return HTTP 200 if name matches else HTTP 500 status
	 */
	@POST
	public Response postName(String name) {
		if (name.equals("heart")) {
			shs.toggleLights();
			return Response.ok("animus " + name).build();
		}
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity("incorrect name, try 'heart'").build();
	}

}
