package com.animushome.heart.happs.examplehapp;

/**
 * Service that will be published on the framework for our hApp. 
 * 
 * In OSGi it's good to split the API and the implementation into interfaces and classes.
 * 
 * See now how this service is consumed by our REST service.
 *
 */
public interface ExampleHAppService {
	
	public void toggleLights();
	
	public void turnOnLights();
	
	public void turnOffLights();

}
